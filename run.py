#!/usr/bin/env python3 
import sys
import os
import os.path

import damitclite.process

def main():
    project_dir = os.getenv("PROJECTDIR")
    module_dir = os.getenv("MODULEDIR")
    print("Running main for ",project_dir)
    d = damitclite.process.DamitCLite(project_dir, module_dir)
    d.run_module()

if __name__ == "__main__":
    main()

