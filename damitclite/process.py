#!/usr/bin/env python3

import yaml
import os
import subprocess

class DamitCLite():
    def __init__(self, project_dir, modules_dir):
        self.project_dir = os.path.realpath(project_dir)
        yaml_path = os.path.join(project_dir, ".damitc.yml")
        self.config = yaml.safe_load(open(yaml_path))
        self.module_name = self.config['module']['name']
        self.module_src = "https://gitlab.com/damitc/"+self.module_name+"-module.git"
        self.modules_dir = os.path.realpath(modules_dir)
        self.module_dir = None
        
        if self.config['output'] == "gitlab-pages":
            self.output_dir = os.path.realpath(os.path.join(project_dir,"public"))
        else:
            self.output_dir = os.path.realpath(os.path.join(project_dir,"tmp"))
        os.makedirs(self.output_dir, exist_ok=True)

    def grab_module(self):
        """ get the module code into a directory """
        os.makedirs(self.modules_dir, exist_ok=True)
 
        self.module_dir = os.path.join(self.modules_dir,self.module_name)
        cmd = "git clone --depth 1 "+self.module_src+" " + self.module_dir
        os.system(cmd)
    
    def run_module(self):
        if not self.module_dir:
            self.grab_module()

        print("Running ",self.project_dir," with ",self.module_name," from ",self.module_dir)
        subprocess.call(
            [
                os.path.join(self.module_dir, "run-project"), 
                self.project_dir, 
                self.module_dir,
                self.output_dir
            ])
        

    def __repr__(self):
        return "DamtC-Lite Project: " + self.project_dir

